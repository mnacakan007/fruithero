import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'main.dart';

// ignore: must_be_immutable
class CheckoutPage extends StatefulWidget {
  List foods;
  double total;

  CheckoutPage(this.foods, this.total);

  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.orange,
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(Icons.arrow_back_ios),
            color: Colors.white,
          ),
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          title: Text('Checkout',
              style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 18.0,
                  color: Colors.white)),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.more_horiz),
              onPressed: () {},
              color: Colors.white,
            )
          ],
        ),
        body: ListView(children: [
          Stack(children: [
            Container(
                height: MediaQuery.of(context).size.height - 82.0,
                width: MediaQuery.of(context).size.width,
                color: Colors.transparent),
            Positioned(
                top: 75.0,
                child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(45.0),
                          topRight: Radius.circular(45.0),
                        ),
                        color: Colors.white),
                    height: MediaQuery.of(context).size.height - 100.0,
                    width: MediaQuery.of(context).size.width)),
            Positioned(
                top: 100.0,
                left: 25.0,
                right: 25.0,
                child: total != 0.00
                    ? Container(
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: foods.length,
                            itemBuilder: (BuildContext context, int index) {
                              return _buildFoodItemTotal(
                                  widget.foods[index].imgPath,
                                  widget.foods[index].foodName,
                                  widget.foods[index].price,
                                  widget.foods[index].orderCounter,
                                  widget.foods[index].totalAmount);
                            }))
                    : _textNotOrder()),
            Positioned(
              bottom: 20.0,
              left: 25.0,
              right: 25.0,
              child: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10.0),
                          topRight: Radius.circular(10.0),
                          bottomLeft: Radius.circular(25.0),
                          bottomRight: Radius.circular(25.0)),
                      color: Colors.blue),
                  height: 50.0,
                  child: Center(
                    child: Text(
                        'Total: ' + '\$' + widget.total.toStringAsFixed(2),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontFamily: 'Montserrat')),
                  ),
                ),
              ),
            )
          ])
        ]));
  }

  Widget _buildFoodItemTotal(String imgPath, String foodName, double price,
      int orderCounter, double totalAmount) {
    return Padding(
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
        child: InkWell(
            child:  orderCounter != 0
                ? Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                    child: Row(children: [
                      Hero(
                          tag: imgPath,
                          child: Image(
                              image: AssetImage(imgPath),
                              fit: BoxFit.cover,
                              height: 75.0,
                              width: 75.0)),
                      SizedBox(width: 10.0),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(foodName,
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 17.0,
                                    fontWeight: FontWeight.bold)),
                            Text('\$' + price.toStringAsFixed(2),
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 15.0,
                                    color: Colors.grey))
                          ])
                    ])),
                    Text(
                        'x' + orderCounter.toString() +
                        ' = '
                        ' \$' +
                        totalAmount.toStringAsFixed(2),
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 15.0,
                        color: Colors.grey))
              ],
            ) : Container()));
  }

  Widget _textNotOrder() {
    return Center(
      child: Container(
        child: Text(
          "You haven’t chosen anything yet.",
          style: TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 18.0,
          ),
        ),
      ),
    );
  }
}
